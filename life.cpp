#include <iostream>
#include <sstream>
#include <string>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <sys/time.h>

#define STDIN 1
#define CPUS 1
#define BIG_NUM 2147483000
#define BOARDS 2
//#define PARTITIONS 256
#define OUT_FILE "lifeS.out"
#define IN_FILE "judge.in"

using namespace std;
typedef unsigned char cell_t; 

cell_t ** allocate_boards (int size) {
	cell_t ** board = (cell_t **) malloc(sizeof(cell_t*)*(size+2));
	int	i, j;
	for (i=0; i<size+2; i++)
		board[i] = (cell_t *) malloc(sizeof(cell_t)*(size+2));
		
	for (i=0; i<size+2; i++) {
		for (j=0; j<size+2; j++)
		{
			board[i][j] = 0;
		}
	}
	return board;
}

void allocate_cols (int ***rowp, int size) {	
	//printf("read1\n");
	rowp[0] = (int **) malloc(sizeof(int*)*size);
	//printf("read2\n");
	int ss = (int)(ceil(sqrt(size)));
	//int ss = size/2 + size/4;
	for(int i=0; i<size; i++){
		//printf("read %d %d %d\n", i, ss, size);
		rowp[0][i] = (int *) malloc(sizeof(int)*ss);
	}
}

struct thread_work_t{
    long unsigned size;
    long unsigned id;
    long PARTITIONS;
    long unsigned steps;
    //long unsigned num_of_threads;
    volatile int * steps_vector;
    volatile int * steps_vector_init;
    bool * steps_locks;
    //int ** colses;
    int *** rowpes;

    cell_t *** boards;
};

void free_boards (cell_t ** board, int size) {
    int i;
    for (i=0; i<size+2; i++)
        free((void*)board[i]);
	free(board);
}

inline int adjacent_to (cell_t ** board, int i, int j) {
	int	count=0;
	
    count+=board[i-1][j-1]&1;
    count+=board[i-1][j]&1;
    count+=board[i-1][j+1]&1;
    count+=board[i][j-1]&1;
    count+=board[i][j+1]&1;
    count+=board[i+1][j-1]&1;
    count+=board[i+1][j]&1;
    count+=board[i+1][j+1]&1;
	

	return count;
}

void prints (cell_t ** board, int size) {
	int i, j;
	char *line;
	line = (char *) malloc(size+4);
	for (i=1; i<size+1; i++) {
		for (j=1; j<size+1; j++)
		{
			line[j-1] = board[i][j] ? 'x' : ' ';
		}
		line[size] = '\0';
		fprintf(stdout, "%s\n", line);
	}
}


void *plays(void *thread_work_uncasted) {
    struct thread_work_t *thread_work = (struct thread_work_t*)thread_work_uncasted;
    const long unsigned int size = thread_work->size;
    const long unsigned int steps = thread_work->steps;
    const long unsigned int id = thread_work->id;
    const long int PARTITIONS = thread_work->PARTITIONS;
    //const long unsigned int num_of_threads = thread_work->num_of_threads;
    volatile int *steps_vector = thread_work->steps_vector;
    volatile int *steps_vector_init = thread_work->steps_vector_init;
    bool *steps_locks = thread_work->steps_locks;
    cell_t *** boards = thread_work->boards;
    cell_t ** c_board;
    cell_t ** n_board;
    //int ** colses = thread_work->colses;
    int *** rowpes = thread_work->rowpes;


    //bool br;
    int	u, s, i, k, j, a, cbx, nbx;// min, repeat=0, est=steps*num_of_threads/100;
    int from, to, ii, jj, len, start, end;
    long int delays = 0;
    cell_t value;
    start = (int) (0.0001 + round(( (float)size/PARTITIONS ) *id) );
	end   = (int) (0.0001 + round(( (float)size/PARTITIONS ) *(id+1)) );

	

    //for(s=0; s<1; s++){
    for(s=0; s<steps; s++){
    	//fprintf(stderr, "%d \n", s);
    	cbx = s%BOARDS;
		nbx = (s+1)%BOARDS;
		c_board = boards[cbx];
    	n_board = boards[nbx];

		//printf("%d \n", s);
		while ( !(steps_vector[id]>=s && steps_vector[id+2]>=s) ) delays++;

		//if(id==3)fprintf(stderr, "y %d %d %d\n", start, end, size);

		for (i=start; i<end; i++) {
			rowpes[nbx][i][0] = 0;
		}

		for (i=start+1; i<end+1; i++) {
		//for (i=0; i<size+2; i++) {
			for (j=0; j<size+2; j++)
			{
				n_board[i][j] = 0;
			}
		}
		steps_vector_init[id+1]++;

		//if(id==3)fprintf(stderr, "x\n");

		while ( !(steps_vector_init[id]>s && steps_vector_init[id+2]>s) ) delays++;


    	//for(k=0; k<size; k++){
    	for(k=start; k<end; k++){
    		//fprintf(stderr, "%d \n", id);
    		to=rowpes[cbx][k][0];
    		for(u=1; u<=to; u++){
    			for(i=k-1; i<=k+1; i++){
    				for(j=rowpes[cbx][k][u]-1; j<=rowpes[cbx][k][u]+1; j++){
    					if(i<0 || j<0 || i>=size || j>=size) continue;
    					//printf("%d,%d\n", i, j);
    					ii=i+1;
    					jj=j+1;
    					if ( c_board[ii][jj] & 2) continue;
    					c_board[ii][jj] = c_board[ii][jj] | 2;
    					a = adjacent_to (c_board, ii, jj);
    					value = 0;
						if (a == 2) value = c_board[ii][jj] & 1;
						if (a == 3) value = 1;
						if(value){
							n_board[ii][jj] = 1;
							value = ++rowpes[nbx][i][0];
							rowpes[nbx][i][ value ] = j;
						}
						OUTERCONTINUE:;
    				}
    			}
    		}
    	}
    	steps_vector[id+1]++;
    }
 //    FILE    *f;

 //    char ss[5];
	// itoa(id, ss, 10);
	// f = fopen(ss, "w");
	// fprintf(f, "%ld", delays);
	// fclose(f);
}

void fprints (cell_t ** board, int size) {
	FILE    *f;
	int i, j;
	f = fopen(OUT_FILE, "w");
	char *line;
	line = (char *) malloc(size+4);
	for (i=1; i<size+1; i++) {
		for (j=1; j<size+1; j++)
		{
			line[j-1] = board[i][j] & 1 ? 'x' : ' ';
		}
		line[size] = '\0';
		fprintf(f, "%s", line);
		fprintf(f, "\n");
	}
	fclose(f);
}


int read_files (FILE * f, cell_t ** board, int **rowp, int size) {
	int	i, j, count=0, count2;
	char *line = (char *) malloc(size+10);
	rowp[0][0]=0;
	for (i=2; i<size+1; i++) {
		fgets(line, size+10, f);
		count2=0;
		for (j=1; j<size+1; j++)
		{
			if(line[j-1]=='x'){
				board[i][j] = 1;
				rowp[i-1][count2+1] = j-1;
				count++;
				count2++;
			}
		}
		rowp[i-1][0] = count2;
	}
}



int main () {
	struct timespec start, end;
	clock_gettime(0, &start);
    int cpus = CPUS;
    if (getenv("MAX_CPUS")) {
        cpus = atoi(getenv("MAX_CPUS"));
    }
    assert(cpus > 0 && cpus <= 64);
    fprintf(stderr, "Running on %d CPUs\n", cpus);
	int size, steps;
	int	i, j;
	FILE *f;
	void *status;
	char line [ 128 ];	
	if(STDIN)
    	f = stdin;
    else
    	f = fopen(IN_FILE, "r");
	fgets(line, 128, f);
	sscanf(line, "%d %d", &size, &steps);
	cell_t *** boards = (cell_t ***) malloc(sizeof(cell_t**)*(BOARDS));
	//int ** colses = (int**)malloc(sizeof(int**)*(BOARDS));
	int *** rowpes = (int***)malloc(sizeof(int**)*(BOARDS));
	int counts[BOARDS];
	
	for(i=0; i<BOARDS; i++){
		
		boards[i] = allocate_boards (size);
		allocate_cols (&rowpes[i], size);
	}


	//TIME
	clock_gettime(0, &end);
	long int delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
	fprintf(stderr, "ALLOC: %lu\n", delta_us);
	clock_gettime(0, &start);
	//TIME



	
	counts[0] = read_files (f, boards[0], rowpes[0], size);
	fclose(f);
	

	//TIME
	clock_gettime(0, &end);
	delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
	fprintf(stderr, "READ: %lu\n", delta_us);
	clock_gettime(0, &start);
	//TIME
	
	
	const int num_of_threads = cpus;
	struct thread_work_t tw[num_of_threads];
	pthread_t thread[num_of_threads];

	const long int PARTITIONS = num_of_threads;

	volatile int *steps_vector;
	steps_vector = (int *) malloc(sizeof(int)*(2+PARTITIONS));
	steps_vector[0] = BIG_NUM; 
	for (i=1; i <= PARTITIONS; i++)
		steps_vector[i]=0;
	steps_vector[PARTITIONS+1] = BIG_NUM;

	volatile int *steps_vector_init;
	steps_vector_init = (int *) malloc(sizeof(int)*(2+PARTITIONS));
	steps_vector_init[0] = BIG_NUM; 
	for (i=1; i <= PARTITIONS; i++)
		steps_vector_init[i]=0;
	steps_vector_init[PARTITIONS+1] = BIG_NUM;

	//TIME
	clock_gettime(0, &end);
	delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
	fprintf(stderr, "MISC: %lu\n", delta_us);
	clock_gettime(0, &start);
	//TIME
	

	for (i=0; i < num_of_threads; i++) {
        tw[i].boards  		    = boards;
        //tw[i].colses  		= colses;
        tw[i].rowpes  		    = rowpes;
        tw[i].size    			= size;
        tw[i].steps   			= steps;
        tw[i].id    			= i;
        tw[i].PARTITIONS    	= PARTITIONS;
        //tw[i].num_of_threads 	= num_of_threads;
        tw[i].steps_vector 		= steps_vector;
        tw[i].steps_vector_init = steps_vector_init;
        //tw[i].steps_locks 		= steps_locks;
        //fprintf(stderr, "Starting thread %d\n", i);
        pthread_create(&thread[i], NULL, plays, (void*)&tw[i]);
    }
    for (i=0; i < num_of_threads; i++) {
        pthread_join(thread[i], &status);
    }

    //TIME
	clock_gettime(0, &end);
	delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
	fprintf(stderr, "FIN: %lu\n", delta_us);
	clock_gettime(0, &start);
	//TIME


	if(STDIN)
		prints(boards[steps%BOARDS], size);
	else
		fprints(boards[steps%BOARDS], size);


	//TIME
	clock_gettime(0, &end);
	delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
	fprintf(stderr, "PRINT FILES: %lu\n", delta_us);
	clock_gettime(0, &start);
	//TIME

	for(i=0; i<BOARDS; i++){
		free_boards(boards[i],size);
	}


	//TIME
	clock_gettime(0, &end);
	delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
	fprintf(stderr, "TOOK: %lu\n", delta_us);
	clock_gettime(0, &start);
	//TIME


	return 0;
}
