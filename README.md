# Concurrent Papers

This repo contains the documentation for the ideas behind the solutions of two parallel programming tasks.
The two tasks were part of the [FCDS](https://tu-dresden.de/ing/informatik/sya/se/studium/lehrveranstaltungen/summer-semester/foundations-of-concurrent-and-distributed-systems-1) course [lab](https://tu-dresden.de/ing/informatik/sya/se/studium/labs-seminars/concurrent_and_distributed_systems_lab) at the TU Dresden.

The two problems (game of life and sudokount) were part of the [11th Marathon of Parallel Programming](http://lspd.mackenzie.br/marathon/16/problems.html) and are described [here](http://lspd.mackenzie.br/marathon/16/problemset.pdf).
