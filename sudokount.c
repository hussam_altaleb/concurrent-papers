#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <math.h>
#include <sys/time.h>
#include <pthread.h>
#include<unistd.h>

#define INT_TYPE unsigned long long 
#define INT_TYPE_SIZE (sizeof(INT_TYPE) * 8)
#define CELL_VAL_SIZE 1
#define MAX_BDIM 8
#define IN_FILE "judge.in"
//#define IN_FILE "sudokount2.in"
#define LOCKS_VECTOR_SIZE 1000


enum SOLVE_STRATEGY {SUDOKU_SOLVE, SUDOKU_COUNT_SOLS};
#define SUDOKU_SOLVE_STRATEGY SUDOKU_COUNT_SOLS

#define BUILD_ERROR_IF(condition) ((void)sizeof(char[1 - 2*!!(condition)]))
static int powi(int a, int b);

int cpus = 4;
int pool = 1;
int max_depth = 0;
int STEPS = 28;
int *buf;
int size;
unsigned long long *sum;
unsigned char *ready_vector;
unsigned char *done_vector;
pthread_mutex_t *locks_vector;
volatile unsigned char done=0;
int buf_size;
volatile long int steps_at_depth = 0;
int return_at_depth = 0;
int *steps_vector;
int **map;
int *map_sums;
unsigned long int idx_map = 0;
unsigned long int idx_rdy = 0;
#define CHUNK 512
#define SIZE_OF_MAP powi(2, 18)/CHUNK
#define SIZE_OF_MAP_SUMS powi(2, 18)

pthread_mutex_t lock, lock_u;
struct timespec start, end;
struct timespec start_p, end_p;
long int delta_us, assign_us = 0, assign_cs = 0, search_cs = 0;
long int elim_us = 0, elim_cs = 0;
void BUILD_TIME_CHECKS() {
    BUILD_ERROR_IF(INT_TYPE_SIZE * CELL_VAL_SIZE < MAX_BDIM * MAX_BDIM);
}

typedef struct cellval {
    INT_TYPE v[CELL_VAL_SIZE];
} cell_v;

typedef struct cell_coord {
    int r,c;
} cell_coord;

typedef struct sudoku {
    int bdim;
    int dim;
    int peers_size;
    int* grid;
    
    cell_coord ****unit_list; 
    cell_coord ***peers;
    cell_v **values;
    
    unsigned long long sol_count;
} sudoku;
static int assign_0 (sudoku *s, int i, int j, int d);
static int search (sudoku *s, int status, int depth, int *v);


static inline int cell_v_get(cell_v *v, int p) {
    return !!((*v).v[(p - 1) / INT_TYPE_SIZE] & (((INT_TYPE)1) << ((p - 1) % INT_TYPE_SIZE))); //!! otherwise p > 32 breaks the return
}

static inline void cell_v_unset(cell_v *v, int p) {
    (*v).v[(p - 1) / INT_TYPE_SIZE] &= ~(((INT_TYPE)1) << ((p - 1) % INT_TYPE_SIZE));
}

static inline void cell_v_set(cell_v *v, int p) {
    (*v).v[(p - 1) / INT_TYPE_SIZE] |= ((INT_TYPE)1) << ((p -1) % INT_TYPE_SIZE);
}

static inline int cell_v_count(cell_v *v) {
    int i, acc = 0;
    for ( i = 0; i < CELL_VAL_SIZE; i++) 
        acc += __builtin_popcountll((*v).v[i]);
    return acc;
}

static inline int digit_get (cell_v *v) {
    int i, count = cell_v_count(v);
    if (count != 1) return -1;
    for ( i = 0; i < CELL_VAL_SIZE; i++) 
        if ((*v).v[i]) return 1 + INT_TYPE_SIZE * i + __builtin_ctzll((*v).v[i]);
    return -1;
}

static void destroy_sudoku(sudoku *s) {
    int i, j, k;
    for ( i = 0; i < s->dim; i++) {
        for ( j = 0; j < s->dim; j++) {
            for ( k = 0; k < 3; k++)
                free(s->unit_list[i][j][k]);
            free(s->unit_list[i][j]);
        }
        free(s->unit_list[i]);
    }
    free(s->unit_list);
    
    for ( i = 0; i < s->dim; i++) {
        for ( j = 0; j < s->dim; j++)
            free(s->peers[i][j]);
        free(s->peers[i]);
    }
    free(s->peers);
    
    for ( i = 0; i < s->dim; i++) 
        free(s->values[i]);
    free(s->values);
    free(s->grid);
    
    free(s);
}

static void init(sudoku *s) {
    int i, j, k, l, pos, ibase, jbase;
    
    for (i = 0; i < s->dim; i++) {
        ibase = i / s->bdim * s->bdim;
        for (j = 0; j < s->dim; j++) {
            for (pos = 0; pos < s->dim; pos++) {
                s->unit_list[i][j][0][pos].r = i; //row 
                s->unit_list[i][j][0][pos].c = pos;
                s->unit_list[i][j][1][pos].r = pos; //column
                s->unit_list[i][j][1][pos].c = j;
            }
            jbase = j / s->bdim * s->bdim;
            for (pos = 0, k = 0; k < s->bdim; k++) //box
                for (l = 0; l < s->bdim; l++, pos++) {
                    s->unit_list[i][j][2][pos].r = ibase + k;
                    s->unit_list[i][j][2][pos].c = jbase + l;
                }
        }
    }
    
    for (i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++) {
            pos = 0;
            for (k = 0; k < s->dim; k++) { //row
                if (s->unit_list[i][j][0][k].c != j)
                    s->peers[i][j][pos++] = s->unit_list[i][j][0][k]; 
            }
            for (k = 0; k < s->dim; k++) { 
                cell_coord sq = s->unit_list[i][j][1][k]; //column
                if (sq.r != i)
                    s->peers[i][j][pos++] = sq; 
                sq = s->unit_list[i][j][2][k]; //box
                if (sq.r != i && sq.c != j)
                    s->peers[i][j][pos++] = sq; 
            }
        }
    assert(pos == s->peers_size);
}

static int parse_grid(sudoku *s) {
    int i, j, k;
    int ld_vals[s->dim][s->dim];
    for (k = 0, i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++, k++) {
            ld_vals[i][j] = s->grid[k];
        }
    
    for (i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++)
            for (k = 1; k <= s->dim; k++)
                cell_v_set(&s->values[i][j], k);
    
    for (i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++)
            if (ld_vals[i][j] > 0 && !assign_0(s, i, j, ld_vals[i][j])){
                return 0;
            }

    return 1;
}

static sudoku *create_sudoku() {
    int bdim = size;
    assert(bdim <= MAX_BDIM);
    
    sudoku *r = malloc(sizeof(sudoku));
    r->bdim = bdim;
    int i, j, k, dim = bdim * bdim;
    r->dim = dim;
    r->peers_size = 3 * dim - 2 * bdim - 1;

    r->grid = (int *) malloc(sizeof(int) * buf_size);
    for (i = 0; i < buf_size; i++) 
        r->grid[i] = buf[i];

    r->sol_count = 0;

    r->unit_list = malloc(sizeof(cell_coord***) * dim);
    assert(r->unit_list);
    for (i = 0; i < dim; i++) {
        r->unit_list[i] = malloc(sizeof(cell_coord**) * dim);
        assert (r->unit_list[i]);
        for (j = 0; j < dim; j++) {
            r->unit_list[i][j] = malloc(sizeof(cell_coord*) * 3);
            assert(r->unit_list[i][j]);
            for (k = 0; k < 3; k++) {
                r->unit_list[i][j][k] = calloc(dim, sizeof(cell_coord));
                assert(r->unit_list[i][j][k]);
            }
        }
    }
    
    r->peers = malloc(sizeof(cell_coord**) * dim);
    assert(r->peers);
    for (i = 0; i < dim; i++) {
        r->peers[i] = malloc(sizeof(cell_coord*) * dim);
        assert(r->peers[i]);
        for (j = 0; j < dim; j++) {
            r->peers[i][j] = calloc(r->peers_size, sizeof(cell_coord));
            assert(r->peers[i][j]);
        }
    }
    
    r->values = malloc (sizeof(cell_v*) * dim);
    assert(r->values);
    for (i = 0; i < dim; i++) {
        r->values[i] = calloc(dim, sizeof(cell_v));
        assert(r->values[i]);
    }
    
    init(r);
    if (!parse_grid(r)) {
        printf("Error parsing grid\n");
        destroy_sudoku(r);
        return 0;
    }
    
    return r;
}

static int eliminate (sudoku *s, int i, int j, int d) {

    int k, ii, cont, pos, ret=1;

    if (!cell_v_get(&s->values[i][j], d)){
        goto F_E;
    }

    cell_v_unset(&s->values[i][j], d);

    int count = cell_v_count(&s->values[i][j]);
    if (count == 0) {
        ret = 0;
        goto F_E;
    } else if (count == 1) {
        for (k = 0; k < s->peers_size; k++){
            if (!eliminate(s, s->peers[i][j][k].r, s->peers[i][j][k].c, digit_get(&s->values[i][j]))){
                ret = 0;
                goto F_E;
            }
        }
    }
    

    for (k = 0; k < 3; k++) {//row, column, box 
        cont = 0;
        pos = 0;
        cell_coord* u = s->unit_list[i][j][k];
        for (ii = 0; ii < s->dim; ii++) {
            if (cell_v_get(&s->values[u[ii].r][u[ii].c], d)) {
                cont++;
                pos = ii;
            }
        }
        if (cont == 0){
            ret = 0;
            goto F_E;
        }
        else if (cont == 1) {
            if (!assign_0( s, u[pos].r, u[pos].c, d)){
                ret = 0;
                goto F_E;
            }
        }
    }
    F_E: return ret;
}


static int assign_0 (sudoku *s, int i, int j, int d) {
    int d2;
    for (d2 = 1; d2 <= s->dim; d2++)
        if (d2 != d) 
            if (!eliminate(s, i, j, d2))
               return 0;
    return 1;
}



static void display(sudoku *s) {
    int i, j;
    printf("%d\n", s->bdim);
    for (i = 0; i < s->dim; i++)
        for (j = 0; j < s->dim; j++)
            printf("%d ",  digit_get(&s->values[i][j]));
}



static int search (sudoku *s, int status, int depth, int *v) {
    int i, j, k;
    if (!status){
        if(depth<STEPS){
            return_at_depth++;
        }
        return status;
    }
    void *status_p;

    int solved = 1;
    for (i = 0; solved && i < s->dim; i++) 
        for (j = 0; j < s->dim; j++) 
            if (cell_v_count(&s->values[i][j]) != 1) {
                solved = 0;
                break;
            }
    if (solved) {
        s->sol_count++;
        return SUDOKU_SOLVE_STRATEGY == SUDOKU_SOLVE;
    }

    //ok, there is still some work to be done
    int min = INT_MAX;
    int minI = -1;
    int minJ = -1;
    int ret = 0;
    
    //cell_v **tmp = s->values;
    cell_v **values_bkp = malloc (sizeof (cell_v *) * s->dim);
    for (i = 0; i < s->dim; i++)
        values_bkp[i] = malloc (sizeof (cell_v) * s->dim);
    
    for (i = 0; i < s->dim; i++) 
        for (j = 0; j < s->dim; j++) {
            int used = cell_v_count(&s->values[i][j]);
            if (used > 1 && used < min) {
                min = used;
                minI = i;
                minJ = j;
            }
        }
    
    int st=1, en=s->dim;
    //printf("_ _ %d %d\n", depth, STEPS);
    if(depth < STEPS){
        st = v[depth];
        en = st;
    }
    for (k = st; k <= en; k++) {
        //printf("_%d\n", k);
        if (cell_v_get(&s->values[minI][minJ], k))  {
            for (i = 0; i < s->dim; i++)
                for (j = 0; j < s->dim; j++)
                    values_bkp[i][j] = s->values[i][j];
            
            
            //if(pool >= cpus){
                int srch_res = search (s, assign_0(s, minI, minJ, k), depth+1, v);
                TRALALA:
                //printf("res: %d\n", srch_res);
                if (srch_res) {
                    ret = 1;
                    goto FR_RT;
                } else {
                    for (i = 0; i < s->dim; i++) 
                        for (j = 0; j < s->dim; j++)
                            s->values[i][j] = values_bkp[i][j];
                    //pthread_mutex_unlock(&lock_u);
                }
           
        }
    }
    
    FR_RT:
    for (i = 0; i < s->dim; i++)
        free(values_bkp[i]);
    free (values_bkp);
    
    return ret;
}

void solve(sudoku *s, int id){
    long int i;
    int u, loop=1;
    while(loop){
        if(done){
            loop--;
        }
        for(u=cpus*(-1); u<0; u++){
            for(i=id+u; i<steps_at_depth; i+=cpus){
                if(i<0 || done_vector[i]) continue;

                pthread_mutex_lock(&locks_vector[i%LOCKS_VECTOR_SIZE]);
                if(done_vector[i]){
                    pthread_mutex_unlock(&locks_vector[i%LOCKS_VECTOR_SIZE]);
                    continue;
                }
                done_vector[i] = 1;
                pthread_mutex_unlock(&locks_vector[i%LOCKS_VECTOR_SIZE]);
                search (s, 1, 0, &map[i/CHUNK][(i%CHUNK)*STEPS]);

            }
        }
    }
    return;
}

struct thread_work_t{
    int id;
};

void *solve_p (void *thread_work_uncasted) {//sudoku *s, int i, int j, int d) {
    struct thread_work_t *thread_work = (struct thread_work_t*)thread_work_uncasted;
    int id = thread_work->id;

    sudoku *s = create_sudoku();
    solve(s, id);
    sum[id] = s->sol_count;
    destroy_sudoku(s);

    fprintf(stderr, "FINISHED: %d with: %lld\n", id, sum[id]);

}

static int determine_valid_helpers (sudoku *s, int status, int kkk, int depth, int m_d) {
    int i, j, k;
    if(depth>max_depth){
        max_depth=depth;
    }
    if(depth<=m_d && depth>0){
        steps_vector[depth-1]=kkk;
    }

    if(depth==STEPS){
        if(idx_rdy%CHUNK==0){
            map[idx_rdy/CHUNK] = (int *) malloc(CHUNK*STEPS * sizeof(int));
        }
        for(i = 0; i<STEPS; i++){
            map[idx_rdy/CHUNK][idx_map%(CHUNK*STEPS)] = steps_vector[i]; 
            idx_map++;
        }
        ready_vector[idx_rdy++] = 1;
        steps_at_depth++;
        return 0;
    }

    if (!status){
        if(depth<m_d){
            return_at_depth++;
        }
        return status;
    }

    int solved = 1;
    for (i = 0; solved && i < s->dim; i++) 
        for (j = 0; j < s->dim; j++) 
            if (cell_v_count(&s->values[i][j]) != 1) {
                solved = 0;
                break;
            }
    if (solved) {
        s->sol_count++;
        return SUDOKU_SOLVE_STRATEGY == SUDOKU_SOLVE;
    }

    int min = INT_MAX;
    int minI = -1;
    int minJ = -1;
    int ret = 0;
    
    cell_v **tmp = s->values;
    cell_v **values_bkp = malloc (sizeof (cell_v *) * s->dim);
    for (i = 0; i < s->dim; i++)
        values_bkp[i] = malloc (sizeof (cell_v) * s->dim);
    
    for (i = 0; i < s->dim; i++) 
        for (j = 0; j < s->dim; j++) {
            int used = cell_v_count(&s->values[i][j]);
            if (used > 1 && used < min) {
                min = used;
                minI = i;
                minJ = j;
            }
        }
    
    int st, en;
    st=1; 
    en=s->dim;
    for (k = st; k <= en; k++) {
        if (cell_v_get(&s->values[minI][minJ], k))  {
            for (i = 0; i < s->dim; i++)
                for (j = 0; j < s->dim; j++)
                    values_bkp[i][j] = s->values[i][j];
            
            int srch_res;
            int assignee = 1;
            assignee = assign_0(s, minI, minJ, k);
            if (determine_valid_helpers (s, assignee, k, depth+1, m_d)) {
                ret = 1;
                goto DT_V;
            } 
            else {
                for (i = 0; i < s->dim; i++) 
                    for (j = 0; j < s->dim; j++){
                        s->values[i][j] = values_bkp[i][j];
                    }
            }
        }
    }    
    DT_V:
    for (i = 0; i < s->dim; i++)
        free(values_bkp[i]);
    free (values_bkp);

    return ret;
}

int determine_valids(sudoku *s) {
    int x = determine_valid_helpers (s, 1, 0, 0, STEPS);
    done = 1;
    return x;
}


int main (int argc, char **argv) {
    fprintf(stderr, "Program Start\n");

    int i;
    map = (int **) malloc(SIZE_OF_MAP * sizeof(int*));
    steps_vector = (int *) calloc(STEPS, sizeof(int));
    ready_vector = (unsigned char *) calloc(SIZE_OF_MAP_SUMS, sizeof(unsigned char));
    done_vector = (unsigned char *) calloc(SIZE_OF_MAP_SUMS, sizeof(unsigned char));
    locks_vector = (pthread_mutex_t *) malloc(LOCKS_VECTOR_SIZE * sizeof(pthread_mutex_t));

    for(i=0; i<LOCKS_VECTOR_SIZE; i++){
        pthread_mutex_init(&locks_vector[i], NULL);
    }

    FILE *f;
    f = fopen(IN_FILE, "r");
    if (getenv("MAX_CPUS")) {
        cpus = atoi(getenv("MAX_CPUS"));
        f = stdin;
        fprintf(stderr, "CPUs read from env: %d\n", cpus);
    }
    else{
        fprintf(stderr, "CPUs NOT read from env: %d\n", cpus);
    }


    assert(cpus > 0 && cpus <= 64);
    if (pthread_mutex_init(&lock, NULL) != 0){
        printf("\n mutex init failed\n");
        return 1;
    }
    if (pthread_mutex_init(&lock_u, NULL) != 0){
        printf("\n mutex init failed\n");
        return 1;
    }


    clock_gettime(0, &start_p);

    assert(fscanf(f, "%d", &size) == 1);
    assert (size <= MAX_BDIM);
    buf_size = size * size * size * size;
    buf = (int *) malloc(sizeof(int) * buf_size);
    sum = (unsigned long long *) malloc(sizeof(unsigned long long) * cpus);

    for (i = 0; i < buf_size; i++) {
        if (fscanf(f, "%d", &buf[i]) != 1) {
            printf("error reading file (%d)\n", i);
            exit(1);
        }
    }


    fprintf(stderr, "Threads Spawned\n");


    struct thread_work_t tw[cpus];
    pthread_t thread[cpus];
    for (i=0; i < cpus-1; i++) {
        tw[i].id       = i;
        pthread_create(&thread[i], NULL, solve_p, (void*)&tw[i]);
    }

    sudoku *s = create_sudoku();
    determine_valids(s);
    
    long long int init_sum = s->sol_count;

    if(max_depth<STEPS){
        printf("%lld\n", init_sum);
        return 0;
    }
    tw[i].id       = i;
    pthread_create(&thread[i], NULL, solve_p, (void*)&tw[i]);

    unsigned long long total_sum=0;
    for (i=0; i < cpus; i++) {
        pthread_join(thread[i], NULL);
        total_sum += sum[i];
    }
    total_sum += init_sum;

    printf("%lld\n", total_sum);
    clock_gettime(0, &end_p);
    delta_us = (end_p.tv_sec - start_p.tv_sec) * 1000000 + (end_p.tv_nsec - start_p.tv_nsec) / 1000;
    fprintf(stderr, "TOOK: %lu\n", delta_us);
    return 0;

    
    fprintf(stderr, "steps_at_depth: %d\n", steps_at_depth);

    fprintf(stderr, "%d %d\n", idx_map, SIZE_OF_MAP);


    free(map);
    free(buf);
    return 0;
}

static int powi(int a, int b){
    int i, c=1;
    for(i=0; i<b; i++)
        c*=a;
    return c;
}
